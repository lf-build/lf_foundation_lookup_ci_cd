﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace LendFoundry.Foundation.Lookup.Api.Controllers
{
    /// <summary>
    /// Represents the lookup api controller class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents the constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public ApiController(ILookupService service, ILogger logger):base(logger)
        {
            Service = service;
        }

        private ILookupService Service { get; }


        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="lookupEntries">The lookup entries.</param>
        /// <returns></returns>
        [HttpPost("/{entity}")]
        [ProducesResponseType(typeof(ILookupEntry[]),200)]
        [ProducesResponseType(typeof(ErrorResult),400)]
        public IActionResult Add(string entity, [FromBody] Dictionary<string, string> lookupEntries)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entity))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entity));
                entity = WebUtility.UrlDecode(entity);
                var entries = Service.Add(entity, lookupEntries);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Deleles the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        [HttpDelete("/{entity}/{code}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult Delele([FromRoute] string entity, [FromRoute] string code)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entity))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entity));
                entity = WebUtility.UrlDecode(entity);

                if (string.IsNullOrWhiteSpace(code))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(code));
                code = WebUtility.UrlDecode(code); ;

                Service.Delete(entity, code);
                return GetStatusCodeResult(204);
            });
        }

        /// <summary>
        /// Deleles all.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        [HttpDelete("/{entity}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult DeleteAll([FromRoute] string entity)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entity))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entity));
                entity = WebUtility.UrlDecode(entity);

                Service.DeleteAll(entity);
                return GetStatusCodeResult(204);
            });
        }

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/entities")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetEntities()
        {
            return Execute(() =>
            {
                var entities = Service.GetEntities();
                return Ok(entities);
            });
        }

        /// <summary>
        /// Gets the lookup entries.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        [HttpGet("/{entity}")]
        [ProducesResponseType(typeof(IDictionary<string, string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetLookupEntries([FromRoute] string entity)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entity))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entity));
                entity = WebUtility.UrlDecode(entity);

                var entries = Service.GetLookupEntries(entity);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Gets the lookup entries.
        /// </summary>
        /// <param name="entities">The entities</param>
        /// <returns></returns>
        [HttpGet("/entities/{*entities}")]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(IDictionary<string, IEnumerable<ILookupEntry>>), 200)]
        public IActionResult GetMultipleLookupEntries([FromRoute] string entities)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entities))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entities));
                entities = WebUtility.UrlDecode(entities);

                var entitiesList = SplitEntities(entities);
                var entries = Service.GetMultipleLookupEntries(entitiesList);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Gets the lookup entry.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        [HttpGet("/{entity}/{code}")]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetLookupEntry([FromRoute] string entity, [FromRoute] string code)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entity))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entity));
                entity = WebUtility.UrlDecode(entity);

                if (string.IsNullOrWhiteSpace(code))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(code));
                code = WebUtility.UrlDecode(code); ;

                var entry = Service.GetLookupEntry(entity, code);
                return Ok(entry);
            });
        }

        /// <summary>
        ///  lookup entry exist or not.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        [HttpGet("/{entity}/{code}/exist")]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult Exist([FromRoute] string entity, [FromRoute] string code)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(entity))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(entity));
                entity = WebUtility.UrlDecode(entity);

                if (string.IsNullOrWhiteSpace(code))
                    throw new ArgumentException("Value cannot be null or whitespace.", nameof(code));
                code = WebUtility.UrlDecode(code); ;

                var entry = Service.Exist(entity, code);
                return Ok(entry);
            });
        }

        // <summary>
        /// Splits the names.
        /// </summary>
        /// <param name="names">The names.</param>
        /// <returns></returns>
        private static List<string> SplitEntities(string entities)
        {
            return string.IsNullOrWhiteSpace(entities)
                ? new List<string>()
                : entities.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private IActionResult GetStatusCodeResult(int statusCode)
        {
#if DOTNET2
            return new StatusCodeResult(statusCode);
#else
            return new HttpStatusCodeResult(statusCode);
#endif
        }

    }
}