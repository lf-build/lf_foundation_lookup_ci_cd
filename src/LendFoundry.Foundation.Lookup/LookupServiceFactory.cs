﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.Foundation.Lookup
{
    public class LookupServiceFactory : ILookupServiceFactory
    {
        public LookupServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ILookupService Create(StaticTokenReader reader, ILogger logger)
        {
            var repositoryFactory = Provider.GetService<ILookupRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            return new LookupService(repository, logger);
        }
    }
}