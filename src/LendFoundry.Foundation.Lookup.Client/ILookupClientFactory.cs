﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Lookup.Client
{
    public interface ILookupClientFactory
    {
        ILookupService Create(ITokenReader reader);
    }
}