﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Lookup
{
    public interface ILookupRepositoryFactory
    {
        ILookupRepository Create(ITokenReader reader);
    }
}