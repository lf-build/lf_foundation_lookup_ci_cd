﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Lookup
{
    public interface ILookupServiceFactory
    {
        ILookupService Create(StaticTokenReader reader, ILogger logger);
    }
}