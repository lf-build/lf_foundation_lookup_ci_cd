﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Foundation.Lookup
{
    public class Settings
    {
        private const string Prefix = "LOOKUP";

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "lookup");

        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "lookup";

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
    }
}