﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Api.Controllers;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Foundation.Lookup.Api.Tests
{
    public class LookupApiControllerTests
    {
        private Mock<ILookupService> LookupService { get; } = new Mock<ILookupService>();

        private ApiController GetTenantController()
        {
            return new ApiController(LookupService.Object, Mock.Of<ILogger>());
        }

        [Fact]
        public void AddReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Add(It.IsAny<string>(), It.IsAny<IDictionary<string, string>>())).Returns(new List<ILookupEntry>() {
               new LookupEntry()
               {
                   Code="a",
                   Value="a",
                   Entity="appplication"
               },
               new LookupEntry()
               {
                   Code="b",
                   Value="b",
                   Entity="appplication"
               }
            });

            var response = controller.Add("application", new System.Collections.Generic.Dictionary<string, string>() { { "a", "a" }, { "b", "b" } });

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response).Value;
            Assert.IsAssignableFrom<IEnumerable<ILookupEntry>>(lookupEntriesRaw);
            List<ILookupEntry> lookupEntries = lookupEntriesRaw as List<ILookupEntry>;
            Assert.NotNull(lookupEntries);
            Assert.Equal(2, lookupEntries.Count);
        }

        [Fact]
        public void AddReturnBadRequestWhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Add(It.IsAny<string>(), It.IsAny<IDictionary<string, string>>())).Throws(new ArgumentException("Invalid entity type"));

            var response = controller.Add(null, new System.Collections.Generic.Dictionary<string, string>() { { "a", "a" }, { "b", "b" } });

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }


        [Fact]
        public void DeleteReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Delete(It.IsAny<string>(), It.IsAny<string>()));

            var response = controller.Delele("application", "a");

            Assert.IsType<StatusCodeResult>(response);
            var lookupEntriesRaw = ((StatusCodeResult)response);
            Assert.Equal(204, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void DeleteReturnBadRequestWhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Delete(It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("Invalid entity type"));

            var response = controller.Delele(null, "a");

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }


        [Fact]
        public void DeleteAllReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.DeleteAll(It.IsAny<string>()));

            var response = controller.DeleteAll("application");

            Assert.IsType<StatusCodeResult>(response);
            var lookupEntriesRaw = ((StatusCodeResult)response);
            Assert.Equal(204, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void DeleteAllReturnBadRequestWhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.DeleteAll(It.IsAny<string>())).Throws(new ArgumentException("Invalid entity type"));

            var response = controller.DeleteAll(null);

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void DeleteAllReturnNotFoundWhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.DeleteAll(It.IsAny<string>())).Throws(new NotFoundException("Entity code not found"));

            var response = controller.DeleteAll("not exist");

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(404, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void GetEntitiesReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetEntities()).Returns(new List<string>() { "a" ,"b"});

            var response = controller.GetEntities();

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response).Value;
            Assert.NotNull(lookupEntriesRaw);

            Assert.IsAssignableFrom<IEnumerable<string>>(lookupEntriesRaw);
            List<string> lookupEntries = lookupEntriesRaw as List<string>;
            Assert.NotNull(lookupEntries);
            Assert.Equal(2, lookupEntries.Count);
        }

        [Fact]
        public void GetLookupEntriesReturnNotFoundWhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetLookupEntries(It.IsAny<string>())).Throws(new NotFoundException("Entity code not found"));

            var response = controller.GetLookupEntries("not exist");

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(404, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void GetLookupEntriesReturnBadRequestWhenNullPassed()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetLookupEntries(It.IsAny<string>())).Throws(new ArgumentException("Entity Is Required"));

            var response = controller.GetLookupEntries("");

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void GetLookupEntriesReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetLookupEntries("test")).Returns(new Dictionary<string, string>() { {"a", "a" }, { "b", "b" } });

            var response = controller.GetLookupEntries("test");

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response).Value;
            Assert.IsAssignableFrom<Dictionary<string,string>>(lookupEntriesRaw);
            Dictionary<string, string> lookupEntries = lookupEntriesRaw as Dictionary<string,string>;
            Assert.NotNull(lookupEntries);
            Assert.Equal(2, lookupEntries.Count);
        }


        [Fact]
        public void GetMultipleLookupEntriesReturnEmptyCollection()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetMultipleLookupEntries(It.IsAny<List<string>>())).Returns(new Dictionary<string, IEnumerable<ILookupEntry>>());

            var response = controller.GetMultipleLookupEntries("not exist/not exist-1");

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response);
            Assert.Equal(200, lookupEntriesRaw.StatusCode);
            var lookupEntries = lookupEntriesRaw.Value as Dictionary<string, IEnumerable<ILookupEntry>>;
            Assert.Empty(lookupEntries);
        }

        [Fact]
        public void GetMultipleLookupEntriesReturnBadRequestWhenNullPassed()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetMultipleLookupEntries(It.IsAny<List<string>>())).Throws(new ArgumentException());

            var response = controller.GetMultipleLookupEntries("");

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = (ErrorResult)response;
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void GetMulitpleLookupEntriesReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetMultipleLookupEntries(new List<string>() { "test", "test2" })).Returns(new Dictionary<string, IEnumerable<ILookupEntry>>() { { "test", new List<ILookupEntry>() { new LookupEntry() { Code = "a", Value = "b" } } }, { "test2", new List<LookupEntry>() { new LookupEntry {  Code = "a", Value = "b" } } } });

            var response = controller.GetMultipleLookupEntries("test/test2");

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response).Value;
            Assert.IsAssignableFrom<Dictionary<string, IEnumerable<ILookupEntry>>>(lookupEntriesRaw);
            Dictionary<string, IEnumerable<ILookupEntry>> lookupEntries = lookupEntriesRaw as Dictionary<string, IEnumerable<ILookupEntry>>;
            Assert.NotNull(lookupEntries);
            Assert.Equal(2, lookupEntries.Count);
        }

        [Fact]
        public void GetLookupEntryReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(new Dictionary<string, string>() { { "a", "b" }  });

            var response = controller.GetLookupEntry("test","test");

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response);
            Assert.Equal(200, lookupEntriesRaw.StatusCode);
            var lookupEntries = lookupEntriesRaw.Value as Dictionary<string, string>;
            Assert.Single(lookupEntries);
        }


        [Fact]
        public void GetLookupEntryReturn404WhenNotFound()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Throws(new NotFoundException("Not found"));

            var response = controller.GetLookupEntry("test", "test");

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = ((ErrorResult)response);
            Assert.Equal(404, lookupEntriesRaw.StatusCode);
        }

        [Fact]
        public void GetLookupEntryReturn400WhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException());

            var response = controller.GetLookupEntry(null,null);

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = ((ErrorResult)response);
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }



        [Fact]
        public void ExistLookupEntryReturnOk()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            var response = controller.Exist("test", "test");

            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response);
            Assert.Equal(200, lookupEntriesRaw.StatusCode);
            Assert.Equal("True", lookupEntriesRaw.Value.ToString());
        }


        [Fact]
        public void ExistLookupEntryReturnFalseWhenNotFound()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Exist(It.IsAny<string>(), It.IsAny<string>())).Returns(false);

            var response = controller.Exist("test", "test");


            Assert.IsType<OkObjectResult>(response);
            var lookupEntriesRaw = ((OkObjectResult)response);
            Assert.Equal(200, lookupEntriesRaw.StatusCode);
            Assert.Equal("False", lookupEntriesRaw.Value.ToString());
        }

        [Fact]
        public void ExistLookupEntryReturn400WhenEntityIsNull()
        {
            var controller = GetTenantController();
            LookupService.Setup(s => s.Exist(It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException());

            var response = controller.Exist(null, null);

            Assert.IsType<ErrorResult>(response);
            var lookupEntriesRaw = ((ErrorResult)response);
            Assert.Equal(400, lookupEntriesRaw.StatusCode);
        }

    }
}
