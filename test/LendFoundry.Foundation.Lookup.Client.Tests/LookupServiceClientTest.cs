using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Foundation.Lookup.Client.Tests
{
    public class LookupServiceClientTest
    {
        private Mock<IServiceClient> ServiceClient { get; } = new Mock<IServiceClient>();
        private int CacheDuration = 10;
        private Mock<ITokenReader> TokenReader { get; } = new Mock<ITokenReader>();
        private Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        public LookupServiceClient GetLookupServiceClient()
        {
            TokenHandler.Setup(s => s.Parse(It.IsAny<string>())).Returns(new Token()
            {
                Tenant = "my-tenant"
            });
            TokenReader.Setup(s => s.Read()).Returns("my-tenant");
            return new LookupServiceClient(ServiceClient.Object, TokenReader.Object, TokenHandler.Object, CacheDuration);
        }

        [Fact]
        public void AddLookupEntry()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<IEnumerable<LookupEntry>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new List<LookupEntry>() { new LookupEntry() { Code = "a", Entity = "application", Value = "a" } });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.Add("application", new Dictionary<string, string>() { { "a", "a" } });

            Assert.Single(response);
        }


        [Fact]
        public void DeleteLookupEntry()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r);

            var lookupService = GetLookupServiceClient();
            lookupService.Delete("entityType", "application");

        }

        [Fact]
        public void DeleteAllLookupEntry()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r);

            var lookupService = GetLookupServiceClient();
            lookupService.DeleteAll("entityType");

        }

        [Fact]
        public void GetEntities()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<List<string>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new List<string>() { "entityType", "documentType" });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.GetEntities();

            Assert.Equal(2, response.Count);
        }


        [Fact]
        public void GetLookupEntries()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<Dictionary<string, string>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new Dictionary<string, string>() { { "application", "application" } });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.GetLookupEntries("application");

            Assert.Single(response);
        }

        [Fact]
        public void GetMultipleLookupEntries()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<Dictionary<string, IEnumerable<ILookupEntry>>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new Dictionary<string, IEnumerable<ILookupEntry>>() { { "entityType", new List<ILookupEntry>() { new LookupEntry() { Code = "application", Value = "application" } } } });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.GetMultipleLookupEntries(new List<string>() { "entityType" });

            Assert.Single(response);
        }

        [Fact]
        public void GetLookupEntry()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<Dictionary<string, string>> (It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new Dictionary<string, string>() { { "application", "application" } });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.GetLookupEntry("entityType","application");

            Assert.Single(response);
            Assert.Equal("application", response.Keys.First());

        }

        [Fact]
        public void Exist()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.ExecuteRequest(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns( new RestResponse() { StatusCode = System.Net.HttpStatusCode.OK, Content = "true" });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.Exist("entityType", "application");

            Assert.True(response);

        }

        [Fact]
        public void ExistReturnFalse()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.ExecuteRequest(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new RestResponse() { StatusCode = System.Net.HttpStatusCode.OK, Content = "false" });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.Exist("entityType", "application");

            Assert.False(response);

        }

        [Fact]
        public void ExistCaseInsensitiveTest()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.ExecuteRequest(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new RestResponse() { StatusCode = System.Net.HttpStatusCode.OK, Content = "FaLSE" });

            var lookupService = GetLookupServiceClient();
            var response = lookupService.Exist("entityType", "application");

            Assert.False(response);

        }

        [Fact]
        public void GetLookupEntryTestCaching()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<Dictionary<string, string>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new Dictionary<string, string>() { { "application", "application" } });

            TokenHandler.Setup(s => s.Parse(It.IsAny<string>())).Returns(new Token()
            {
                Tenant = "my-tenant"
            });
            TokenReader.Setup(s => s.Read()).Returns("my-tenant");

            var lookupServiceClient = GetLookupServiceClient();
            var response = lookupServiceClient.GetLookupEntry("entityType", "application");
            Assert.Single(response);
            Assert.Equal("application", response.Keys.First());
            ServiceClient.Verify(s => s.Execute<Dictionary<string, string>>(It.IsAny<IRestRequest>()), Times.Once);
            ServiceClient.ResetCalls();

            response = lookupServiceClient.GetLookupEntry("entityType", "application");
            Assert.Single(response);
            Assert.Equal("application", response.Keys.First());
            ServiceClient.Verify(s => s.Execute<Dictionary<string, string>>(It.IsAny<IRestRequest>()), Times.Never);
            ServiceClient.ResetCalls();

            Task.Delay(TimeSpan.FromSeconds(CacheDuration + 10)).Wait();

            response = lookupServiceClient.GetLookupEntry("entityType", "application");
            Assert.Single(response);
            Assert.Equal("application", response.Keys.First());
            ServiceClient.Verify(s => s.Execute<Dictionary<string, string>>(It.IsAny<IRestRequest>()), Times.Once);
            ServiceClient.ResetCalls();

        }
    }
}
