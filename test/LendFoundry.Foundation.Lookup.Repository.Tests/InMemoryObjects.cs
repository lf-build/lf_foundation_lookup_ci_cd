﻿using LendFoundry.Tenant.Client;
using Moq;
using System.Collections.Generic;

namespace LendFoundry.Foundation.Lookup.Repository.Tests
{
    public class InMemoryObjects
    {
        protected IDictionary<string, string> FakeLookupEntries => new Dictionary<string, string>
        {
            { "201", "Bank of America" },
            { "202", "Bank of Irvine" },
            { "203", "Bank of C.A." },
            { "204", "Bank of L.A." },
            { "205", "Bank of LendFoundry" }
        };

        protected ILookupEntry InMemoryLookupEntry => new LookupEntry
        {
            Entity = "Entity",
            Code = "Code",
            Value = "Value"
        };

        protected ITenantService TenantService
        {
            get
            {
                var mock = new Mock<ITenantService>();
                mock.Setup(t => t.Current).Returns(new TenantInfo()
                {
                    Id = "my-tenant",
                    IsActive = true
                });
                return mock.Object;
            }
        }
    }
}