﻿using LendFoundry.Foundation.Lookup.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Foundation.Lookup.Repository.Tests
{
    public class LookupRepositoryTest : InMemoryObjects
    {
        private ILookupRepository Repository(IMongoConfiguration config)
        {
            return new LookupRepository(TenantService, config);
        }

        [MongoFact]
        public void Add_WhenExecutionOk()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"00{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });

                // act
                var lookupEntriesAdded = repo.Add("UnitTest", FakeLookupEntries);

                // assert
                Assert.NotNull(lookupEntriesAdded);
                Assert.NotEmpty(lookupEntriesAdded);
                Assert.Equal(lookupEntriesAdded.First().Entity, "UnitTest");
            });
        }

        [MongoFact]
        public void Add_When_Raise_A_InvalidOperationException()
        {
            MongoTest.Run(config =>
            {
                Assert.Throws<InvalidOperationException>(() =>
                {
                    // arrange
                    var repo = Repository(config);

                    // arrange
                    var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                    .Select((lookup, i) =>
                    {
                        ++i;
                        lookup.Code = $"00{i}";
                        lookup.Value = $"Description of Test 00{i}";
                        lookup.Entity = "UnitTest";

                        return lookup;
                    });

                    // act
                    var lookupEntriesAdded = repo.Add("UnitTest", FakeLookupEntries);
                    repo.Add("UnitTest", FakeLookupEntries);

                    // assert
                    Assert.NotNull(lookupEntriesAdded);
                    Assert.NotEmpty(lookupEntriesAdded);
                    Assert.Equal(lookupEntriesAdded.First().Entity, "UnitTest");
                });
            });
        }

        [MongoFact]
        public void Delete_WhenExecutionOk()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"20{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });

                // act
                var lookupEntriesAdded = repo.Add("UnitTest", FakeLookupEntries);
                var result = repo.Delete("UnitTest", "201");

                // assert
                Assert.NotNull(lookupEntriesAdded);
                Assert.NotEmpty(lookupEntriesAdded);
                Assert.Equal(lookupEntriesAdded.First().Entity, "UnitTest");

                Assert.True(result);
            });
        }

        [MongoFact]
        public void Delete_When_NotFoundRecord()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"20{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });

                // act
                var lookupEntriesAdded = repo.Add("UnitTest", FakeLookupEntries);
                var result = repo.Delete("UnitTest", "208");

                // assert
                Assert.NotNull(lookupEntriesAdded);
                Assert.NotEmpty(lookupEntriesAdded);
                Assert.Equal(lookupEntriesAdded.First().Entity, "UnitTest");

                Assert.False(result);
            });
        }

        [MongoFact]
        public void DeleteAll_WhenExecutionOk()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"20{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });

                // act
                var lookupEntriesAdded = repo.Add("UnitTest", FakeLookupEntries);
                var result = repo.DeleteAll("UnitTest");
                var removedItems = repo.GetLookupEntries("UnitTest");

                // assert
                Assert.NotNull(lookupEntriesAdded);
                Assert.NotEmpty(lookupEntriesAdded);
                Assert.Equal(lookupEntriesAdded.First().Entity, "UnitTest");

                Assert.True(result);
                Assert.Empty(removedItems);
            });
        }

        [MongoFact]
        public void DeleteAll_When_NotFoundRecords()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"20{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });

                // act
                var lookupEntriesAdded = repo.Add("UnitTest", FakeLookupEntries);
                var result = repo.DeleteAll("UnitTest-");
                var removedItems = repo.GetLookupEntries("UnitTest");

                // assert
                Assert.NotNull(lookupEntriesAdded);
                Assert.NotEmpty(lookupEntriesAdded);
                Assert.Equal(lookupEntriesAdded.First().Entity, "UnitTest");

                Assert.False(result);
                Assert.NotEmpty(removedItems);
            });
        }

        [MongoFact]
        public void GetLookupEntries_WhenExecutionOk()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"00{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });
                repo.Add("UnitTest", FakeLookupEntries);

                // act

                var lookupEntries = repo.GetLookupEntries("UnitTest");

                // assert
                Assert.NotNull(lookupEntries);
                Assert.NotEmpty(lookupEntries);
                Assert.Equal(lookupEntries.First().Entity, "UnitTest");
            });
        }

        [MongoFact]
        public void GetLookupEntries_When_NotFoundRecords()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"00{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });
                repo.Add("UnitTest", FakeLookupEntries);

                // act

                var lookupEntries = repo.GetLookupEntries("UnitTest-");

                // assert
                Assert.NotNull(lookupEntries);
                Assert.Empty(lookupEntries);
            });
        }

        [MongoFact]
        public void GetLookupEntry_WhenExecutionOk()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"00{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });
                repo.Add("UnitTest", FakeLookupEntries);

                // act

                var lookupEntry = repo.GetLookupEntry("UnitTest", "202");

                // assert
                Assert.NotNull(lookupEntry);
                Assert.Equal(lookupEntry.Entity, "UnitTest");
                Assert.Equal(lookupEntry.Code, "202");
            });
        }

        [MongoFact]
        public void GetLookupEntry_When_NotFoundRecords()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var repo = Repository(config);

                // arrange
                var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
                .Select((lookup, i) =>
                {
                    ++i;
                    lookup.Code = $"00{i}";
                    lookup.Value = $"Description of Test 00{i}";
                    lookup.Entity = "UnitTest";

                    return lookup;
                });
                repo.Add("UnitTest", FakeLookupEntries);

                // act

                var lookupEntry = repo.GetLookupEntry("UnitTest", "300");

                // assert
                Assert.Null(lookupEntry);
            });
        }
    }
}