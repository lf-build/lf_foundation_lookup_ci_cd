﻿using LendFoundry.Foundation.Logging;
using Moq;
using System.Collections.Generic;

namespace LendFoundry.Foundation.Lookup.Tests
{
    public class InMemoryObjects
    {
        // logger
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        // repository
        protected Mock<ILookupRepositoryFactory> LookupRepositoryFactory { get; } = new Mock<ILookupRepositoryFactory>();

        protected Mock<ILookupRepository> LookupRepository { get; } = new Mock<ILookupRepository>();

        // service
        protected Mock<ILookupServiceFactory> LookupServiceFactory { get; } = new Mock<ILookupServiceFactory>();

        protected ILookupService LookupService => new LookupService(LookupRepository.Object, Logger.Object);

        protected Dictionary<string, string> FakeLookupEntries => new Dictionary<string, string>
        {
            { "201", "Bank of America" },
            { "202", "Bank of Irvine" },
            { "203", "Bank of C.A." },
            { "204", "Bank of L.A." },
            { "205", "Bank of LendFoundry" }
        };

        protected ILookupEntry InMemoryLookupEntry => new LookupEntry
        {
            Entity = "Entity",
            Code = "Code",
            Value = "Value"
        };
    }
}