﻿using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Foundation.Lookup.Tests
{
    public class LookupServiceTest : InMemoryObjects
    {
        [Fact]
        public void Add_WhenExecutionOk()
        {
            // arrange
            var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
            .Select((lookup, i) =>
            {
                ++i;
                lookup.Code = $"00{i}";
                lookup.Value = $"Description of Test 00{i}";
                lookup.Entity = "UnitTest";

                return lookup;
            });

            LookupRepository.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Returns(entries);
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            var lookupEntriesAdded = LookupService.Add("UnitTest", FakeLookupEntries);

            // assert
            Assert.NotNull(lookupEntriesAdded);
            Assert.NotEmpty(lookupEntriesAdded);
            Assert.Equal("UnitTest", lookupEntriesAdded.First().Entity);

            LookupRepository.Verify(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()), Times.Once);
        }

        [Fact]
        public void Add_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.Add(null, FakeLookupEntries);

                // assert
                LookupRepository.Verify(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()), Times.Never);
            });
        }

        [Fact]
        public void Add_When_HasNoLookEntries()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.Add("UnitTest", null);

                // assert
                LookupRepository.Verify(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()), Times.Never);
            });
        }

        [Fact]
        public void Add_When_CodeIsDuplicated()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>())).Throws<InvalidOperationException>();
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.Add("UnitTest", FakeLookupEntries);

                // assert
                LookupRepository.Verify(x => x.Add(It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()), Times.Once);
            });
        }

        [Fact]
        public void Delete_WhenExecutionOk()
        {
            // arrange
            LookupRepository.Setup(x => x.Delete(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Logger.Setup(x => x.Info(It.IsAny<string>()));

            // act
            LookupService.Delete("UnitTest", "201");

            // assert
            LookupRepository.Verify(x => x.Delete(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void Delete_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.Delete(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));

                // act
                LookupService.Delete(null, "201");

                // assert
                LookupRepository.Verify(x => x.Delete(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public void Delete_When_HasNoCode()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.Delete(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));

                // act
                LookupService.Delete("UnitTest", null);

                // assert
                LookupRepository.Verify(x => x.Delete(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public void Delete_When_NotFoundRecord()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.Delete(It.IsAny<string>(), It.IsAny<string>())).Returns(false);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));

                // act
                LookupService.Delete("UnitTest", "201");

                // assert
                LookupRepository.Verify(x => x.Delete(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            });
        }

        [Fact]
        public void DeleteAll_WhenExecutionOk()
        {
            // arrange
            LookupRepository.Setup(x => x.DeleteAll(It.IsAny<string>())).Returns(true);
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            Logger.Setup(x => x.Info(It.IsAny<string>()));

            // act
            LookupService.DeleteAll("UnitTest");

            // assert
            LookupRepository.Verify(x => x.DeleteAll(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void DeleteAll_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.DeleteAll(It.IsAny<string>())).Returns(true);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));

                // act
                LookupService.DeleteAll(null);

                // assert
                LookupRepository.Verify(x => x.DeleteAll(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public void DeleteAll_When_NotFoundRecord()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.DeleteAll(It.IsAny<string>())).Returns(false);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
                Logger.Setup(x => x.Info(It.IsAny<string>()));

                // act
                LookupService.DeleteAll("UnitTest");

                // assert
                LookupRepository.Verify(x => x.DeleteAll(It.IsAny<string>()), Times.Once);
            });
        }

        [Fact]
        public void GetLookupEntries_WhenExecutionOk()
        {
            // arrange
            var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
            .Select((lookup, i) =>
            {
                ++i;
                lookup.Code = $"00{i}";
                lookup.Value = $"Description of Test 00{i}";
                lookup.Entity = "UnitTest";

                return lookup;
            });

            LookupRepository.Setup(x => x.GetLookupEntries(It.IsAny<string>())).Returns(entries);
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            var lookupEntries = LookupService.GetLookupEntries("UnitTest");

            // assert
            Assert.NotNull(lookupEntries);
            Assert.NotEmpty(lookupEntries);
            Assert.Equal("001", lookupEntries.First().Key);

            LookupRepository.Verify(x => x.GetLookupEntries(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void GetLookupEntries_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntries(It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.GetLookupEntries(null);

                // assert
                LookupRepository.Verify(x => x.GetLookupEntries(It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public void GetLookupEntries_When_NotFoundRecord()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntries(It.IsAny<string>())).Returns(new List<ILookupEntry>().AsEnumerable);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.GetLookupEntries("Test");

                // assert
                LookupRepository.Verify(x => x.GetLookupEntries(It.IsAny<string>()), Times.Once);
            });
        }

        [Fact]
        public void GetLookupEntry_WhenExecutionOk()
        {
            // arrange
            var entries = new[] { InMemoryLookupEntry }
            .Select((lookup, i) =>
            {
                ++i;
                lookup.Code = $"00{i}";
                lookup.Value = $"Description of Test 00{i}";
                lookup.Entity = "UnitTest";

                return lookup;
            });

            LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(entries.First());
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            var lookupEntry = LookupService.GetLookupEntry("UnitTest", "001");

            // assert
            Assert.NotNull(lookupEntry);
            Assert.NotEmpty(lookupEntry);
            Assert.Equal("001",lookupEntry.First().Key);

            LookupRepository.Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void GetLookupEntry_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.GetLookupEntry(null, "001");

                // assert
                LookupRepository.Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public void GetLookupEntry_When_HasNoCode()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.GetLookupEntry("UnitTest", null);

                // assert
                LookupRepository.Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            });
        }

        [Fact]
        public void GetLookupEntry_When_NotFoundRecord()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                ILookupEntry fakeLookupEntry = null;
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(fakeLookupEntry);
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                LookupService.GetLookupEntries("Test");

                // assert
                LookupRepository.Verify(x => x.GetLookupEntries(It.IsAny<string>()), Times.Once);
            });
        }


        [Fact]
        public void IsExistLookupEntry_WhenExecutionOk()
        {
            // arrange
            var entries = new[] { InMemoryLookupEntry }
            .Select((lookup, i) =>
            {
                ++i;
                lookup.Code = $"00{i}";
                lookup.Value = $"Description of Test 00{i}";
                lookup.Entity = "UnitTest";

                return lookup;
            });

            LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(entries.First());
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            var lookupEntry = LookupService.Exist("UnitTest", "001");

            // assert
            Assert.True(lookupEntry);

            LookupRepository.Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void IsExistLookupEntry_WhenExecutionOk_ReturnFalse()
        {
            // arrange
            var entries = new[] { InMemoryLookupEntry }
            .Select((lookup, i) =>
            {
                ++i;
                lookup.Code = $"00{i}";
                lookup.Value = $"Description of Test 00{i}";
                lookup.Entity = "UnitTest";

                return lookup;
            });

            LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns((ILookupEntry)null);
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            var lookupEntry = LookupService.Exist("UnitTest", "notexist");

            // assert
            Assert.False(lookupEntry);

            LookupRepository.Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void IsExistLookupEntry_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                var response =LookupService.Exist(null, "001");

                // assert
                Assert.False(response);

            });
        }

        [Fact]
        public void IsExistLookupEntry_When_HasNoCode()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                var response=LookupService.Exist("UnitTest", null);

                // assert
                LookupRepository.Verify(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            });
        }

        
        [Fact]
        public void GetMultipleLookupEntries_When_HasNoEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                var response =LookupService.GetMultipleLookupEntries(null);

            });
        }

        [Fact]
        public void GetMultipleLookupEntries_When_HasEmptyEntity()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                LookupRepository.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()));
                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                var response = LookupService.GetMultipleLookupEntries(new List<string>());

            });
        }

        [Fact]
        public void GetLookupEntriesMultiple_WhenExecutionOk()
        {
            // arrange
            var entries = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
            .Select((lookup, i) =>
            {
                ++i;
                lookup.Code = $"00{i}";
                lookup.Value = $"Description of Test 00{i}";
                lookup.Entity = "UnitTest";

                return lookup;
            });

            var entries2 = new[] { InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry, InMemoryLookupEntry }
.Select((lookup, i) =>
{
    ++i;
    lookup.Code = $"00{i}";
    lookup.Value = $"Description of Test 00{i}";
    lookup.Entity = "UnitTest2";

    return lookup;
});
            LookupRepository.Setup(x => x.GetLookupEntries("UnitTest")).Returns(entries);
            LookupRepository.Setup(x => x.GetLookupEntries("UnitTest2")).Returns(entries2);

            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            var lookupEntries = LookupService.GetMultipleLookupEntries(new List<string>() { "UnitTest", "UnitTest2" });

            // assert
            Assert.NotNull(lookupEntries);
            Assert.NotEmpty(lookupEntries);
            Assert.Equal(2, lookupEntries.Count());
            LookupRepository.Verify(x => x.GetLookupEntries(It.IsAny<string>()), Times.Exactly(2));
        }

    }
}